window.onload = function () {
    let imgs = document.querySelectorAll(".photo-gallery__item");
    let imgStyles = document.querySelectorAll(".photo-gallery__img-style");
    let overlayBigImg = document.getElementById("overlay__big-img");
    let overlay = document.getElementById("overlay-id");
    let isClicked = false;

    function scaling(img) {
        img.style.zIndex = "3";
        img.animate([{
                transform: "scale(1)"
            },
            {
                transform: "scale(2)"
            }
        ], 500);

        img.style.transform = "scale(2)";
        console.log(img);
    };

    function notScaling(img) {
        img.style.transform = "scale(1)";
        img.style.zIndex = "0";
    };

    imgs.forEach(i => {
        i.onmouseover = function (e) {
            scaling(i);
        };
        i.onmouseout = function (e) {
            notScaling(i);
        };
    });

    imgStyles.forEach(i => {
        i.onmousedown = function (e) {
            if (!isClicked) {
                i.parentElement.style.transform = "scale(1)";

                overlayBigImg.classList.add(i.classList.item(0));
                overlayBigImg.classList.add("big-img");

                overlay.style.opacity = "1";
                overlay.style.zIndex = "6";

                isClicked = true;
            }
        };
    });

    imgStyles.forEach(i => {
        i.onmouseover = function (e) {
            if (isClicked) {
                e.preventDefault();
                e.stopPropagation();
            }
        };
        i.onmouseout = function (e) {
            if (isClicked) {
                e.preventDefault();
                e.stopPropagation();
            }
        };
    });

    overlay.onclick = function (e) {
        overlayBigImg.classList.remove(overlayBigImg.classList.item(1));
        overlay.style.opacity = "0";

        imgStyles.forEach(i => {
            if (isClicked) {
                overlayBigImg.classList.remove("big-img");
                overlay.style.zIndex = "0";
                isClicked = false;
            };
        });
    };
}