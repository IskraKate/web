const data = localStorage.getItem('form')
const jsonData = JSON.parse(data)

if (jsonData) {
  let emailInput = document.getElementById('email'),
    passwordInput = document.getElementById('password'),
    passwordRepeatInput = document.getElementById('password_repeat')

  emailInput.value = jsonData.email
  passwordInput.value = jsonData.password
  passwordRepeatInput.value = jsonData.password
}
