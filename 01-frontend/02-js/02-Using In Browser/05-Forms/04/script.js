function CustomValidation() {
  this.invalidities = []
  this.validityChecks = []
}

CustomValidation.prototype = {
  addInvalidity: function (message) {
    this.invalidities.push(message)
  },
  getInvalidities: function () {
    return this.invalidities.join('. \n')
  },
  checkValidity: function (input) {
    for (let i = 0; i < this.validityChecks.length; i++) {
      let isInvalid = this.validityChecks[i].isInvalid(input)
      if (isInvalid) {
        this.addInvalidity(this.validityChecks[i].invalidityMessage)
      }

      let requirementElement = this.validityChecks[i].element
      if (requirementElement) {
        if (isInvalid) {
          requirementElement.classList.add('invalid')
          requirementElement.classList.remove('valid')
        } else {
          requirementElement.classList.remove('invalid')
          requirementElement.classList.add('valid')
        }
      }
    }
  },
}

let emailValidityChecks = [
  {
    isInvalid: function (input) {
      return input.value.length < 12
    },
    invalidityMessage: 'This input needs to be at least 12 characters',
    element: document.querySelector(
      'label[for="email"] .input-requirements li:nth-child(1)'
    ),
  },
  {
    isInvalid: function (input) {
      return !/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(input.value)
    },
    invalidityMessage: 'email must be like: smth@gmail.com',
    element: document.querySelector(
      'label[for="email"] .input-requirements li:nth-child(2)'
    ),
  },
]

let passwordValidityChecks = [
  {
    isInvalid: function (input) {
      return (input.value.length < 8) | (input.value.length > 100)
    },
    invalidityMessage: 'This input needs to be between 8 and 100 characters',
    element: document.querySelector(
      'label[for="password"] .input-requirements li:nth-child(1)'
    ),
  },
  {
    isInvalid: function (input) {
      return !input.value.match(/[0-9]/g)
    },
    invalidityMessage: 'At least 1 number is required',
    element: document.querySelector(
      'label[for="password"] .input-requirements li:nth-child(2)'
    ),
  },
  {
    isInvalid: function (input) {
      return !input.value.match(/[a-z]/g)
    },
    invalidityMessage: 'At least 1 lowercase letter is required',
    element: document.querySelector(
      'label[for="password"] .input-requirements li:nth-child(3)'
    ),
  },
  {
    isInvalid: function (input) {
      return !input.value.match(/[A-Z]/g)
    },
    invalidityMessage: 'At least 1 uppercase letter is required',
    element: document.querySelector(
      'label[for="password"] .input-requirements li:nth-child(4)'
    ),
  },
  {
    isInvalid: function (input) {
      return !input.value.match(/[\!\@\#\$\%\^\&\*]/g)
    },
    invalidityMessage: 'You need one of the required special characters',
    element: document.querySelector(
      'label[for="password"] .input-requirements li:nth-child(5)'
    ),
  },
]

let passwordRepeatValidityChecks = [
  {
    isInvalid: function () {
      return passwordRepeatInput.value != passwordInput.value
    },
    invalidityMessage: 'This password needs to match the first one',
  },
]

function checkInput(input) {
  input.CustomValidation.invalidities = []
  input.CustomValidation.checkValidity(input)

  if (
    input.CustomValidation.invalidities.length == 0 &&
    (input.value != '' || input.checked)
  ) {
    input.setCustomValidity('')
    return true
  } else {
    let message = input.CustomValidation.getInvalidities()
    input.setCustomValidity(message)
    return false
  }
}

let passwordInput = document.getElementById('password'),
  passwordRepeatInput = document.getElementById('password_repeat'),
  emailInput = document.getElementById('email')

passwordInput.CustomValidation = new CustomValidation()
passwordInput.CustomValidation.validityChecks = passwordValidityChecks

passwordRepeatInput.CustomValidation = new CustomValidation()
passwordRepeatInput.CustomValidation.validityChecks = passwordRepeatValidityChecks

emailInput.CustomValidation = new CustomValidation()
emailInput.CustomValidation.validityChecks = emailValidityChecks

let inputs = document.querySelectorAll('input:not([type="submit"])'),
  submit = document.querySelector('input[type="submit"]')

for (let i = 0; i < inputs.length; i++) {
  inputs[i].addEventListener('keyup', function () {
    checkInput(this)
  })
}

function saveToStorage() {
  const formData = {
    email: emailInput.value,
    password: password.value,
  }

  const oldData = localStorage.getItem('form')
  const oldDataJSON = JSON.parse(oldData)
  if (oldDataJSON) {
    if (oldDataJSON.email !== formData.email) {
      localStorage.setItem('form', JSON.stringify(formData))
    }
  } else {
    localStorage.setItem('form', JSON.stringify(formData))
  }
}

submit.addEventListener('click', function () {
  let inputsValid = false
  for (let i = 0; i < inputs.length; i++) {
    inputsValid = checkInput(inputs[i])
  }

  if (inputsValid) saveToStorage()
})
