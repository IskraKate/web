const data = localStorage.getItem('form')
const jsonData = JSON.parse(data)

if (!jsonData) window.location.replace('./index.html')

let email = document.getElementById('email'),
  firstnameInput = document.getElementById('firstname'),
  lastnameInput = document.getElementById('lastname'),
  birthdayInput = document.getElementById('birthday'),
  phoneInput = document.getElementById('phone'),
  genderSelect = document.getElementById('gender'),
  skypeInput = document.getElementById('skype')

email.innerHTML = 'Hello, ' + jsonData.email + '!'
firstnameInput.value = jsonData.firstname || ''
lastnameInput.value = jsonData.lastname || ''
birthdayInput.value = jsonData.birthday || ''
phoneInput.value = jsonData.phone || ''
genderSelect.value = jsonData.gender || ''
skypeInput.value = jsonData.skype || ''

function CustomValidation() {
  this.invalidities = []
  this.validityChecks = []
}

CustomValidation.prototype = {
  addInvalidity: function (message) {
    this.invalidities.push(message)
  },
  getInvalidities: function () {
    return this.invalidities.join('. \n')
  },
  checkValidity: function (input) {
    for (let i = 0; i < this.validityChecks.length; i++) {
      let isInvalid = this.validityChecks[i].isInvalid(input)
      if (isInvalid) {
        this.addInvalidity(this.validityChecks[i].invalidityMessage)
      }

      let requirementElement = this.validityChecks[i].element
      if (requirementElement) {
        if (isInvalid) {
          requirementElement.classList.add('invalid')
          requirementElement.classList.remove('valid')
        } else {
          requirementElement.classList.remove('invalid')
          requirementElement.classList.add('valid')
        }
      }
    }
  },
}

let firstnameValidityChecks = [
  {
    isInvalid: function () {
      return firstnameInput.value.length > 20
    },
    invalidityMessage: 'Name needs to be no longer than 20 symbols',
    element: document.querySelector(
      'label[for="password"] .input-requirements li:nth-child(1)'
    ),
  },
  {
    isInvalid: function () {
      return !firstnameInput.value.match('[a-zA-Z]+')
    },
    invalidityMessage: 'Only letters',
  },
]

let lastnameValidityChecks = [
  {
    isInvalid: function () {
      return lastnameInput.value.length > 20
    },
    invalidityMessage: 'Name needs to be no longer than 20 symbols',
  },
  {
    isInvalid: function () {
      return !lastnameInput.value.match('[a-zA-Z]+')
    },
    invalidityMessage: 'Only letters',
  },
]

let birthdayValidityChecks = [
  {
    isInvalid: function () {
      const date = new Date(birthdayInput.value)
      if (date instanceof Date && isNaN(date)) return true

      return date < 1900 || date > new Date()
    },
    invalidityMessage:
      'The date needs to be bigger than 1900 and lesser then nowday.',
  },
]

let phoneValidityChecks = [
  {
    isInvalid: function () {
      return phoneInput.value.length < 10 || phoneInput.value.length > 12
    },
    invalidityMessage:
      'The phone length needs to be bigger than 10 and lesser then 12',
  },
  {
    isInvalid: function () {
      return !phoneInput.value.match(/^[0-9]+$/)
    },
    invalidityMessage: 'Only digits',
  },
]

let genderValidityChecks = [
  {
    isInvalid: function () {
      return genderSelect.value === ''
    },
    invalidityMessage: 'Select gender',
  },
]

let skypeValidityChecks = [
  {
    isInvalid: function () {
      return false
    },
    invalidityMessage: '',
  },
]

function checkInput(input) {
  input.CustomValidation.invalidities = []
  input.CustomValidation.checkValidity(input)

  if (
    input.CustomValidation.invalidities.length == 0 &&
    (input.value != '' || input.checked)
  ) {
    input.setCustomValidity('')
    return true
  } else {
    let message = input.CustomValidation.getInvalidities()
    input.setCustomValidity(message)
    return false
  }
}

firstnameInput.CustomValidation = new CustomValidation()
firstnameInput.CustomValidation.validityChecks = firstnameValidityChecks

lastnameInput.CustomValidation = new CustomValidation()
lastnameInput.CustomValidation.validityChecks = lastnameValidityChecks

birthdayInput.CustomValidation = new CustomValidation()
birthdayInput.CustomValidation.validityChecks = birthdayValidityChecks

phoneInput.CustomValidation = new CustomValidation()
phoneInput.CustomValidation.validityChecks = phoneValidityChecks

genderSelect.CustomValidation = new CustomValidation()
genderSelect.CustomValidation.validityChecks = genderValidityChecks

skypeInput.CustomValidation = new CustomValidation()
skypeInput.CustomValidation.validityChecks = skypeValidityChecks

let inputs = document.querySelectorAll('input:not([type="submit"])'),
  submit = document.querySelector('input[type="submit"]'),
  save = document.getElementById('savebutton')

for (let i = 0; i < inputs.length; i++) {
  inputs[i].addEventListener('keyup', function () {
    checkInput(this)
  })
}

function saveToStorage() {
  const data = localStorage.getItem('form')
  const jsonData = JSON.parse(data)

  const formData = {
    ...jsonData,
    firstname: firstnameInput.value,
    lastname: lastnameInput.value,
    birthday: birthdayInput.value,
    phone: phoneInput.value,
    gender: genderSelect.value,
    skype: skypeInput.value,
  }

  localStorage.setItem('form', JSON.stringify(formData))

  alert('Saved')
}

function removeData() {
  localStorage.removeItem('form')
  alert('Log out')
  window.location.replace('./index.html')
}

save.addEventListener('click', function () {
  let inputsValid = true
  for (let i = 0; i < inputs.length; i++) {
    inputsValid = checkInput(inputs[i])
    if (!inputsValid) break
  }

  if (inputsValid) saveToStorage()
  else alert('Check fields for correct values.')
})
