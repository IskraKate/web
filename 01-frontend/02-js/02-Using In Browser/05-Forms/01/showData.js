function loadFromStorage() {
  return JSON.parse(localStorage.getItem("form"));
}

const data = loadFromStorage();

let username = document.getElementById("username"),
  password = document.getElementById("password"),
  position = document.getElementById("position"),
  specialization = document.getElementById("specialization"),
  sex = document.getElementById("sex");

let specStr = "";
for (let i = 0; i < data.spec.length; i++) {
  specStr += data.spec[i] + " ";
}

username.innerHTML = data.username;
password.innerHTML = data.password;
position.innerHTML = data.position;
specialization.innerHTML = specStr;
sex.innerHTML = data.sex;
