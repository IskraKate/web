function CustomValidation() {
  this.invalidities = []
  this.validityChecks = []
}

CustomValidation.prototype = {
  addInvalidity: function (message) {
    this.invalidities.push(message)
  },
  getInvalidities: function () {
    return this.invalidities.join('. \n')
  },
  checkValidity: function (input) {
    for (let i = 0; i < this.validityChecks.length; i++) {
      let isInvalid = this.validityChecks[i].isInvalid(input)
      if (isInvalid) {
        this.addInvalidity(this.validityChecks[i].invalidityMessage)
      }

      let requirementElement = this.validityChecks[i].element
      if (requirementElement) {
        if (isInvalid) {
          requirementElement.classList.add('invalid')
          requirementElement.classList.remove('valid')
        } else {
          requirementElement.classList.remove('invalid')
          requirementElement.classList.add('valid')
        }
      }
    }
  },
}

let usernameValidityChecks = [
  {
    isInvalid: function (input) {
      return input.value.length < 3
    },
    invalidityMessage: 'This input needs to be at least 3 characters',
    element: document.querySelector(
      'label[for="username"] .input-requirements li:nth-child(1)'
    ),
  },
  {
    isInvalid: function (input) {
      let illegalCharacters = input.value.match(/[^a-zA-Z0-9]/g)
      return illegalCharacters ? true : false
    },
    invalidityMessage: 'Only letters and numbers are allowed',
    element: document.querySelector(
      'label[for="username"] .input-requirements li:nth-child(2)'
    ),
  },
]

let passwordValidityChecks = [
  {
    isInvalid: function (input) {
      return (input.value.length < 8) | (input.value.length > 100)
    },
    invalidityMessage: 'This input needs to be between 8 and 100 characters',
    element: document.querySelector(
      'label[for="password"] .input-requirements li:nth-child(1)'
    ),
  },
  {
    isInvalid: function (input) {
      return !input.value.match(/[0-9]/g)
    },
    invalidityMessage: 'At least 1 number is required',
    element: document.querySelector(
      'label[for="password"] .input-requirements li:nth-child(2)'
    ),
  },
  {
    isInvalid: function (input) {
      return !input.value.match(/[a-z]/g)
    },
    invalidityMessage: 'At least 1 lowercase letter is required',
    element: document.querySelector(
      'label[for="password"] .input-requirements li:nth-child(3)'
    ),
  },
  {
    isInvalid: function (input) {
      return !input.value.match(/[A-Z]/g)
    },
    invalidityMessage: 'At least 1 uppercase letter is required',
    element: document.querySelector(
      'label[for="password"] .input-requirements li:nth-child(4)'
    ),
  },
  {
    isInvalid: function (input) {
      return !input.value.match(/[\!\@\#\$\%\^\&\*]/g)
    },
    invalidityMessage: 'You need one of the required special characters',
    element: document.querySelector(
      'label[for="password"] .input-requirements li:nth-child(5)'
    ),
  },
]

let passwordRepeatValidityChecks = [
  {
    isInvalid: function () {
      return passwordRepeatInput.value != passwordInput.value
    },
    invalidityMessage: 'This password needs to match the first one',
  },
]

let nameValidityChecks = [
  {
    isInvalid: function (input) {
      return (input.value.length < 8) | (input.value.length > 30)
    },
    invalidityMessage: 'This input needs to be between 8 and 30 characters',
    element: document.querySelector(
      'label[for="password"] .input-requirements li:nth-child(1)'
    ),
  },
]

let checkboxValidityChecks = [
  {
    isInvalid: function () {
      for (let i = 0; i < checkBoxes.length; i++) {
        if (checkBoxes[i].checked) return false
      }

      return true
    },
    invalidityMessage: 'You should choose at least one of these',
    element: document.querySelector(
      'label[for="specialization"] .input-requirements li:nth-child(1)'
    ),
  },
]

let radioValidityChecks = [
  {
    isInvalid: function () {
      for (let i = 0; i < radioButtons.length; i++) {
        if (radioButtons[i].checked) return false
      }

      return true
    },
    invalidityMessage: 'Choose your sex, please',
    element: document.querySelector(
      'label[for="sex"] .input-requirements li:nth-child(1)'
    ),
  },
]

let positionValidityChecks = [
  {
    isInvalid: function () {
      if (positionInput.value === 'Choose Your Position') return true
      else return false
    },
    invalidityMessage: 'Choose Your Position',
    element: document.querySelector(
      'label[for="position"] .input-requirements li:nth-child(1)'
    ),
  },
]

let emailValidityChecks = [
  {
    isInvalid: function (input) {
      return (input.value.length < 8) | (input.value.length > 25)
    },
    invalidityMessage: 'This input needs to be between 8 and 25 characters',
    element: document.querySelector(
      'label[for="password"] .input-requirements li:nth-child(1)'
    ),
  },
]

function checkInput(input) {
  input.CustomValidation.invalidities = []
  input.CustomValidation.checkValidity(input)

  if (
    input.CustomValidation.invalidities.length == 0 &&
    (input.value != '' || input.checked)
  ) {
    input.setCustomValidity('')
    return true
  } else {
    let message = input.CustomValidation.getInvalidities()
    input.setCustomValidity(message)
    return false
  }
}

function checkRadioInput(radio) {
  radio.CustomValidation.invalidities = []
  radio.CustomValidation.checkValidity(radio)

  if (radio.CustomValidation.invalidities.length == 0 && radio.checked) {
    radio.setCustomValidity('')
  } else {
    let message = radio.CustomValidation.getInvalidities()
    radio.setCustomValidity(message)
  }
}

let usernameInput = document.getElementById('username'),
  passwordInput = document.getElementById('password'),
  passwordRepeatInput = document.getElementById('password_repeat'),
  nameInput = document.getElementById('name'),
  positionInput = document.getElementById('position'),
  emailInput = document.getElementById('email'),
  checkBoxes = document.querySelectorAll('input[type="checkbox"]'),
  radioButtons = document.querySelectorAll('input[type="radio"]'),
  additionalInput = document.getElementById('info')

usernameInput.CustomValidation = new CustomValidation()
usernameInput.CustomValidation.validityChecks = usernameValidityChecks

passwordInput.CustomValidation = new CustomValidation()
passwordInput.CustomValidation.validityChecks = passwordValidityChecks

passwordRepeatInput.CustomValidation = new CustomValidation()
passwordRepeatInput.CustomValidation.validityChecks = passwordRepeatValidityChecks

nameInput.CustomValidation = new CustomValidation()
nameInput.CustomValidation.validityChecks = nameValidityChecks

positionInput.CustomValidation = new CustomValidation()
positionInput.CustomValidation.validityChecks = positionValidityChecks

emailInput.CustomValidation = new CustomValidation()
emailInput.CustomValidation.validityChecks = emailValidityChecks

checkBoxes.forEach((cb) => {
  cb.CustomValidation = new CustomValidation()
  cb.CustomValidation.validityChecks = checkboxValidityChecks
})

radioButtons.forEach((rb) => {
  rb.CustomValidation = new CustomValidation()
  rb.CustomValidation.validityChecks = radioValidityChecks
})

let inputs = document.querySelectorAll('input:not([type="submit"])'),
  submit = document.querySelector('input[type="submit"]')

for (let i = 0; i < inputs.length; i++) {
  inputs[i].addEventListener('keyup', function () {
    checkInput(this)
  })
}
for (let i = 0; i < checkBoxes.length; i++) {
  checkBoxes[i].addEventListener('change', function () {
    checkInput(this)
  })
}
for (let i = 0; i < radioButtons.length; i++) {
  radioButtons[i].addEventListener('change', function () {
    checkInput(this)
  })
}

function saveToStorage() {
  const languages = []
  const langCheckboxes = document.querySelectorAll(
    'input[type="checkbox"]:checked'
  )

  for (let i = 0; i < langCheckboxes.length; i++) {
    languages.push(langCheckboxes[i].value)
  }

  const formData = {
    username: usernameInput.value,
    password: password.value,
    position: position.value,
    name: nameInput.value,
    email: emailInput.value,
    lang: languages,
    sex:
      document.querySelectorAll('input[type="radio"]:checked').value === 'male'
        ? 'male'
        : 'female',
    additionalInfo: additionalInput.value,
  }

  localStorage.setItem('form', JSON.stringify(formData))
}

submit.addEventListener('click', function () {
  let inputsValid = false
  for (let i = 0; i < inputs.length; i++) {
    inputsValid = checkInput(inputs[i])
  }
  inputsValid = checkInput(positionInput)

  let checkboxesValid = false
  for (let i = 0; i < checkBoxes.length; i++) {
    checkboxesValid = checkInput(checkBoxes[i])

    if (checkboxesValid) break
  }

  let radioValid = false
  for (let i = 0; i < radioButtons.length; i++) {
    radioValid = checkInput(radioButtons[i])

    if (radioValid) break
  }

  if (inputsValid && checkboxesValid && radioValid) saveToStorage()
})
