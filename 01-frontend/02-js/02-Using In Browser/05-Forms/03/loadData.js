const data = localStorage.getItem('form')
const jsonData = JSON.parse(data)

if (jsonData) {
  let usernameInput = document.getElementById('username'),
    passwordInput = document.getElementById('password'),
    passwordRepeatInput = document.getElementById('password_repeat'),
    nameInput = document.getElementById('name'),
    positionInput = document.getElementById('position'),
    emailInput = document.getElementById('email'),
    checkBoxes = document.querySelectorAll('input[type="checkbox"]'),
    radioButtons = document.querySelectorAll('input[type="radio"]'),
    additionalInput = document.getElementById('info')

  usernameInput.value = jsonData.username
  passwordInput.value = jsonData.password
  passwordRepeatInput.value = jsonData.password
  nameInput.value = jsonData.name
  emailInput.value = jsonData.email
  positionInput.value = jsonData.position
  additionalInput.value = jsonData.additionalInfo ? jsonData.additionalInfo : ''

  for (let i = 0; i < checkBoxes.length; i++) {
    for (let j = 0; j < jsonData.lang.length; j++) {
      if (checkBoxes[i].value === jsonData.lang[j]) {
        checkBoxes[i].checked = true
        break
      }
    }
  }

  for (let i = 0; i < radioButtons.length; i++) {
    if (jsonData.sex === radioButtons[i].value) radioButtons[i].checked = true
  }
}
