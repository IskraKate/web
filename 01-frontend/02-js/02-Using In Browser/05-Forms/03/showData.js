const data = localStorage.getItem('form')
const jsonData = JSON.parse(data)

if (jsonData) {
  let usernameInput = document.getElementById('username'),
    passwordInput = document.getElementById('password'),
    nameInput = document.getElementById('name'),
    languagesInput = document.getElementById('languages'),
    positionInput = document.getElementById('position'),
    emailInput = document.getElementById('email'),
    sexInput = document.getElementById('sex'),
    additionalInput = document.getElementById('info')

  usernameInput.innerHTML = jsonData.username
  passwordInput.innerHTML = jsonData.password
  nameInput.innerHTML = jsonData.name
  emailInput.innerHTML = jsonData.email
  positionInput.innerHTML = jsonData.position
  additionalInput.innerHTML = jsonData.additionalInfo
    ? jsonData.additionalInfo
    : ''
  languagesInput.innerHTML = jsonData.lang
  sexInput.innerHTML = jsonData.sex
}
