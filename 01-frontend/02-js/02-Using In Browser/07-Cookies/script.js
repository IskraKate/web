(function () {
  function Question(question, answers, rightAnswers) {
    this.question = question;
    this.answers = answers;
    this.rightAnswers = rightAnswers;
  }

  function Test(questions) {
    this.questions = questions;
    this.currentAnswers = document.querySelectorAll('li');
    this.currentQuestion = document.getElementById('question');
  }

  Test.prototype = {
    checkAnswer: function (a, j) {
      let question = this.questions[j];
      let isRight = true;

      if (a.length !== question.rightAnswers.length) {
        return !isRight;
      } else {
        for (let i = 0; i < a.length; i++) {
          for (let k = 0; k < question.rightAnswers.length; k++) {
            if (question.answers[a[i]] === question.rightAnswers[k]) {
              isRight = true;
              break;
            } else {
              isRight = false;
            }
          }

          if (isRight === false) {
            return isRight;
          }
        }
      }
      return isRight;
    },
    nextQuestion: function (j) {
      if (j < this.questions.length) {
        this.currentQuestion.innerText = this.questions[j].question;
        for (let i = 0; i < this.currentAnswers.length; i++) {
          this.currentAnswers[i].querySelector(
            'span'
          ).innerText = this.questions[j].answers[i];
        }
      }
    },
  };

  function Init() {
    let questions = [],
      j = 0,
      rightAnswers = 0,
      li = document.querySelectorAll('li'),
      time = document.querySelector('span.changeT'),
      button = document.getElementById('btn'),
      checkboxes = [],
      answers = [],
      timer = 60500,
      t = 60,
      name = 'аноним';

    questions = AddQuestions();

    if (localStorage.length > 0) {
      let key = localStorage.key(0);
      alert(`${key}: ${localStorage.getItem(key)}`);
    }

    let test = new Test(questions);
    test.nextQuestion(j);

    const intervalId = setInterval(() => {
      t--;
      time.innerText = t;
    }, 1000);

    const timerId = setTimeout(() => {
      alert('Ваше время вышло! :<');
      clearInterval(intervalId);
      Init();
    }, timer);

    for (let i = 0; i < li.length; i++) {
      checkboxes.push(li[i].querySelector('input.answer'));
    }

    for (let i = 0; i < checkboxes.length; i++) {
      checkboxes[i].addEventListener('change', function () {
        if (this.checked) {
          answers.push(i);
        } else {
          answers = answers.filter((a) => a !== i);
        }
      });
    }

    button.onclick = function () {
      if (j < questions.length) {
        let isRight = test.checkAnswer(answers, j);

        if (isRight) {
          rightAnswers++;
        }

        j++;
        test.nextQuestion(j);
      }

      if (j === questions.length) {
        name = prompt('Как вас зовут?', '');
        alert(
          'Поздравляем, ' +
            `${name}` +
            ' вы прошлы тест! Правильных ответов: ' +
            `${rightAnswers}` +
            ' из: ' +
            `${questions.length}`
        );
        localStorage.setItem(name, rightAnswers);
        Init();
        clearTimeout(timerId);
        clearInterval(intervalId);
      }

      for (let i = 0; i < checkboxes.length; i++) {
        checkboxes[i].checked = false;
      }

      answers = [];
    };
  }

  function AddQuestions() {
    let questions = [];
    questions.push(
      new Question(
        'Жизнерадостный на английском...',
        ['cheerful', 'resilient', 'greedy', 'dummy'],
        ['cheerful', 'resilient']
      )
    );
    questions.push(
      new Question(
        'Умный на английском...',
        ['smart', 'rude', 'clever', 'peachy'],
        ['smart', 'clever']
      )
    );
    questions.push(
      new Question(
        'Забавный на английском...',
        ['funny', 'sad', 'nice', 'small'],
        ['funny']
      )
    );
    questions.push(
      new Question(
        'Блестящий на английском...',
        ['bitter', 'mad', 'beautiful', 'sparkly'],
        ['sparkly']
      )
    );

    return questions;
  }

  window.addEventListener(
    'load',
    function () {
      Init();
    },
    false
  );
})();
