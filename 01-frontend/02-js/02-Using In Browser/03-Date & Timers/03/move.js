let cursor = document.getElementById("cursor");

let setCursorPosition = function (e) {
    let xPosition = e.clientX - cursor.clientWidth / 2 + "px";
    let yPosition = e.clientY - cursor.clientHeight / 2 + "px";
    cursor.style.transform ="translate(" + xPosition + "," + yPosition + ")";
    return {
        x: xPosition,
        y: yPosition
    };
};

document.addEventListener("mousemove", e => setCursorPosition(e));
let scaleCursor = function (e) {
    setCursorPosition(e);
    cursor.style.transform = "translate(" + setCursorPosition(e).x +"," +setCursorPosition(e).y +") ";
}