let bg = document.getElementById("bg");
let grid = document.getElementById("grid");

let date = new Date();
let month = date.getMonth() + 1;
let path = "./images/";

if (month > 11 || month < 3) {
    path += "winter.jpg";
} else if (month > 2 && month < 6) {
    path += "spring.jpg";
} else if (month > 5 && month < 9) {
    path += "summer.jpg";
} else {
    path += "autumn.jpg";
}

bg.style.backgroundImage = "url(" + `${path}` + ")";
bg.style.backgroundRepeat = "no-repeat";
bg.style.backgroundSize = "cover";

let days = new Date(date.getFullYear(), month, 0).getDate();
console.log(days);

let weekDays = ["Sun", "Mon", "Tue", "Wen", "Thu", "Fri", "Sat"];

for (let i = 0; i < weekDays.length; i++) {
    cell = document.createElement("div");
    cell.innerText = weekDays[i];
    cell.classList.add("weekCell");
    grid.appendChild(cell);
}

let curWeekDay = new Date(date.getFullYear(), month - 1, 1).getDay();

for (let i = 0; i < curWeekDay; i++) {
    cell = document.createElement("div");
    cell.classList.add("inactiveCell");
    grid.appendChild(cell);
}

for (let i = 0; i < days; i++) {
    cell = document.createElement("div");
    cell.innerText = i + 1;
    cell.classList.add("cell");
    grid.appendChild(cell);
}

for (let i = days; i < 40; i++) {
    cell = document.createElement("div");
    cell.classList.add("inactiveCell");
    grid.appendChild(cell);
}