<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>NumToStr</title>
    <style>
        body {
            font-family: Verdana, Arial, san-serif;
            font-size: 10pt;
        }
    </style>
</head>

<body>

    <?php

    $units = array('один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять');
    $ten1 = array('одинадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать');
    $ten2 = array('двадцать ', 'тридцать ', 'сорок ', 'пятьдесят ', 'шестьдесят ', 'семьдесят ', 'восемьдесят ', 'девяносто ');
    $hundreds = array('сто ', 'двести ', 'триста ', 'четыреста ', 'пятьсот ', 'шестьсот ', 'семьсот ', 'восемьсот ', 'девятьсот ');
    $thousand = 'тысячи ';
    $thousands = array('одна ', 'две ', 'три ', 'четыре ', 'пять ', 'шесть ', 'семь ', 'восемь ', 'девять ');

    $num = '2345';

    $nums = str_split($num, 1);

    $str = '';
    $isGreater = false;

    for ($i = 0; $i < count($nums); ++$i) {
        switch ($i) {
            case 0:
                $str = $thousands[$nums[$i] - 1] . $thousand;
                break;
            case 1:
                $str = $str . $hundreds[$nums[$i] - 1];
                break;
            case 2:
                if ($nums[$i] > 1) {
                    $isGreater = true;
                    $str = $str . $ten2[$nums[$i] - 2];
                } else {
                    $str = $str . $ten1[$nums[$i + 1] - 1];
                }
                break;
            case 3:
                if ($isGreater) {
                    $str = $str . $units[$nums[$i] - 1];
                }
                break;
        }
    }

    print_r($str);
    ?>

</body>

</html>